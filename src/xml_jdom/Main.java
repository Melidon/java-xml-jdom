package xml_jdom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Main {

	private static Document getSAXParsedDocument(final String fileName) {
		SAXBuilder builder = new SAXBuilder();
		Document document = null;
		try {
			document = builder.build(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	public static int countTags(Element element) {
		int counter = 0;
		List<Element> children = element.getChildren();
		for (Element child : children) {
			counter += countTags(child);
		}
		return 1 + counter;
	}

	public static void keepBusStopsOnly(Document document) {
		List<Element> children = document.getRootElement().getChildren();
		List<Element> shallowCopy = new ArrayList<Element>(children);
		for (Element child : shallowCopy) {
			boolean keep = false;
			List<Element> tags = child.getChildren("tag");
			for (Element tag : tags) {
				if (tag.getAttribute("v").getValue().equals("bus_stop")) {
					keep = true;
					break;
				}
			}
			if (!keep) {
				child.detach();
			}
		}
	}

	public static void saveDocumentTo(Document document, String fileName) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
		try {
			xmlOutputter.output(document, fos);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static double dist1(double lat1, double lon1, double lat2, double lon2) {
		double R = 6371000; // metres
		double phi1 = Math.toRadians(lat1);
		double phi2 = Math.toRadians(lat2);
		double dphi = phi2 - phi1;
		double dl = Math.toRadians(lon2 - lon1);
		double a = Math.sin(dphi / 2) * Math.sin(dphi / 2)
				+ Math.cos(phi1) * Math.cos(phi2) * Math.sin(dl / 2) * Math.sin(dl / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;
		return d;
	}

	public static void addDistanceFrom(Document document, double lat1, double lon1) {
		List<Element> children = document.getRootElement().getChildren();
		for (Element child : children) {
			double lat2;
			double lon2;
			try {
				lat2 = child.getAttribute("lat").getDoubleValue();
				lon2 = child.getAttribute("lon").getDoubleValue();
			} catch (DataConversionException e) {
				e.printStackTrace();
				continue;
			}
			;
			Element distance = new Element("tag");
			distance.setAttribute(new Attribute("k", "distance"));
			distance.setAttribute(new Attribute("v", Double.toString(dist1(lat1, lon1, lat2, lon2))));
			child.addContent(distance);
		}
	}

	public static void main(String[] args) {
		// Argumentumok kezelése
		String inputFileName = null, outputFileName = null;
		double lat = 0, lon = 0;
		for (int i = 0; i < args.length; ++i) {
			switch (args[i]) {
			case "-i":
				inputFileName = args[++i];
				break;
			case "-o":
				outputFileName = args[++i];
				break;
			case "-lat":
				lat = Double.parseDouble(args[++i]);
				break;
			case "-lon":
				lon = Double.parseDouble(args[++i]);
				break;
			default:
				break;
			}
		}

		// Előkészületek
		Document document = getSAXParsedDocument(inputFileName);

		// Első feladat
		System.out.println(countTags(document.getRootElement()));

		// Második feladat
		keepBusStopsOnly(document);
		// saveDocumentTo(document, outputFileName);

		// Harmadik feladat
		addDistanceFrom(document, lat, lon);
		saveDocumentTo(document, outputFileName);
	}

}
